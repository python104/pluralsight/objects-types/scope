count = 0  #globaly decleared.
def show_count():
    print (count)

def set_count(c):
    count = c

show_count()
# what happens here is python look for local namespace (), doesnt find it so look up on global namespace, found it
set_count(5)
# failed to update the value of count bcz assignment in set_count() binds the object (count) to c (5) and 
#no look up perform in the global. We have created variable that prevent access to global variable and shadows the global.
show_count()
##--------Solution-----##
def set_count2(c):
    global count
    count = c
set_count2(5)
show_count()


#this will  resolve the count to the count defined in moduel namespace.